﻿# Créé par Vincent, le 01/06/2016 en Python 3.2

import random

#tab1 = [random.randint(1,20) for k in range(6)]
tab1 = [6,4,5,8,3,2]
tab2 = tab1[:]

def triSelection(tab):
    """
    Reference de cet algo : Dowek page 300.

    Compteurs utilises dans l'algo :

    partie deja triee    | partie non triee (a droite)
    .. | .. | .. | (...) | i  | .. | ..
                         |         j : compteur pour chercher le min dans la
                         |             plage d'indices i..15
                         |     k (indice du min actuellement trouve dans i..j)
    """
    # Preparation de l'affichage
    print("tableau de depart :")
    print(tab)
    print()
    print("Etapes du tri par selection :")
    nbComparaisons = 0


    for i in range(0,len(tab)-1):
        k = i
        for j in range(i+1, len(tab)):
            if tab[j] < tab[k] : # ici on fait une comparaison
                k = j
            nbComparaisons += 1
        valeurTemporaire = tab[i]
        tab[i] = tab[k]
        tab[k] = valeurTemporaire
        print(tab) # Ici on affiche une etape du tri
    print(nbComparaisons, "comparaisons.") # Ici on affiche une etape du tri


def triABulles(tab):
    """
    Reference de cet algo : Dowek exercice 21.4 p. 301.
    """
    # Preparation de l'affichage
    print("tableau de depart :")
    print(tab)
    print()
    print("Etapes du tri a bulles :")
    nbComparaisons = 0

    for nbValeursTriees in range(0,len(tab)):
        for j in range(0, len(tab)-1-nbValeursTriees):
            if tab[j] > tab[j+1]:
                variableTemporaire = tab[j]
                tab[j] = tab[j+1]
                tab[j+1] = variableTemporaire
                print(tab) # Ici on affiche une etape du tri
            nbComparaisons += 1
    print(nbComparaisons, "comparaisons.")

triSelection(tab1)
print()
triABulles(tab2)
