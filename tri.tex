\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=1.5cm,bottom=1.5cm]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}

\usepackage{color}
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\ttfamily\small,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  otherkeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=4,                       % sets default tabsize to 2 spaces
  title=\lstname,                   % show the filename of files included with \lstinputlisting; also try caption instead of title
   belowskip=-1em,
}

\usepackage{fancybox}

\title{I.S.N. -- Algorithmes de tri}

\date{}

\newcommand\moncadre[1]
{
  \noindent\fbox{
    \begin{minipage}{\textwidth}
      #1
    \end{minipage}}
}

\begin{document}

\maketitle

\section{Découvrir un algorithme de tri}

\subsection{Préparation}

Dans la feuille qui vous a été distribuée, découper les cartes numérotés de 1 à 10, et enlever les coins noirs (voir image ci-dessous).

\begin{center}
\includegraphics[width=10cm]{cartes-1.jpg}
\end{center}

Dans l'image suivante, la ligne du bas contient 8 cases grises qui représentent un tableau informatique. Au dessus, on trouve une case qui représente un espace de stockage temporaire de carte.

\begin{center}
\includegraphics[width=10cm]{cartes-2.jpg}
\end{center}

\begin{itemize}
\item Retourner les cartes, puis les mélanger.
\item Enlever 2 cartes (on ne les utilisera pas) afin qu'on ne puisse pas savoir à l'avance la plus petite valeur et la plus grande valeur des 8 cartes utilisées.
\item Poser les 8 cartes restantes sur les cases du tableau gris.
\end{itemize}


\subsection{Trouver comment trier}

Trier les cartes, en respectant les règles suivantes:

\begin{itemize}
\item On peut retourner \textbf{au maximum 2 cartes non triées à la fois}.
\item Les cartes déjà triées peuvent être laissées face visibles.
\item On ne peut pas échanger deux cartes directement. Il faut d'abord en déposer une dans l'espace de stockage temporaire.
\end{itemize}


\subsection{Communiquer}

Expliquer à un autre élève votre méthode de tri.

\subsection{Comparer l'efficacité des algorithmes de tri}

Au sein de la classe, y a-t-il des algorithmes de tri plus efficaces que les autres ?

\pagebreak

\section{Visualiser les algorithmes de tri}

Exemples d'algorithmes de tri:

\begin{itemize}
\item Tri à bulles :  \url{http://www.youtube.com/watch?v=lyZQPjUT5B4}

\includegraphics[width=10cm]{Bubble-sort-with-hungarian}

ou encore :  \url{http://www.youtube.com/watch?v=UnK5ueUgc88}

\item Tri par sélection :  \url{http://www.youtube.com/watch?v=Ns4TPTC8whw}

ou encore :  \url{http://www.youtube.com/watch?v=TW3_7cD9L1A}
\item Tri par insertion :  \url{http://www.youtube.com/watch?v=ROalU379l3U}

ou encore :  \url{http://www.youtube.com/watch?v=Fr0SmtN0IJM}
\end{itemize}

Pour une animation comparative, voir aussi :  \url{http://www.sorting-algorithms.com}

Illustrations sonores :  \url{http://youtu.be/t8g-iYGHpEA}

\section{Programmer un algorithme de tri}

\subsection{Le tri à bulles}

Principe: le tri à bulles consiste à faire remonter une \og bulle \fg{} qui balaie le tableau de la première case à la dernière. Cette bulle emmène vers la droite la valeur la plus haute rencontrée par celle-ci lors de sa remontée à chaque étape.

\begin{lstlisting}[language=Python, linewidth=10cm]
tableau de depart :
[6, 4, 5, 8, 3, 2]

Etapes du tri a bulles :
[4, 6, 5, 8, 3, 2]
[4, 5, 6, 8, 3, 2]
[4, 5, 6, 3, 8, 2]
[4, 5, 6, 3, 2, 8]
[4, 5, 3, 6, 2, 8]
[4, 5, 3, 2, 6, 8]
[4, 3, 5, 2, 6, 8]
[4, 3, 2, 5, 6, 8]
[3, 4, 2, 5, 6, 8]
[3, 2, 4, 5, 6, 8]
[2, 3, 4, 5, 6, 8]
15 comparaisons.
\end{lstlisting}

\subsection{Le tri par sélection}

Principe: On cherche la plus petite valeur du tableau, on l'échange avec la première. Puis on recommence avec le tableau formé par les $n-1$ valeurs suivantes.

\begin{lstlisting}[language=Python, linewidth=10cm]
tableau de depart :
[6, 4, 5, 8, 3, 2]

Etapes du tri par selection :
[2, 4, 5, 8, 3, 6]
[2, 3, 5, 8, 4, 6]
[2, 3, 4, 8, 5, 6]
[2, 3, 4, 5, 8, 6]
[2, 3, 4, 5, 6, 8]
15 comparaisons.
\end{lstlisting}



\end{document}